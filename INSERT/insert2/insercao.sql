-- insercao

USE mydb2;

INSERT INTO funcao(nome, salario)
     VALUES ('Lixador de motor', 2700.12),
	        ('Gerenciador de maquinas frontais', 586.16),
	        ('Alternador do fluxo de simetria', 5678.25),
	        ('Pacificador de tensoes politicas', 10000.86);

select * from funcao;

INSERT INTO pais(sigla_id, nome)
     VALUES ('BR', 'Brasil'),
            ('US', 'Estados Unidos'),
            ('AR', 'Argentina');

INSERT INTO uf(pais_id, sigla_id, nome)
     VALUES ('BR', 'PB', 'Paraiba'),
            ('BR', 'PE', 'Pernambuco'),
            ('AR', 'SL', 'San Luis'),
            ('AR', 'SF', 'Santa Fe'),
            ('BR', 'AC', 'Acre'),
            ('BR', 'RS', 'Rio Grande do Sul'),
            ('BR', 'RN', 'Rio Grande do Norte'),
            ('BR', 'SP', 'Sao Paulo'),
            ('AR', 'BA', 'Provincia de Buenos Aires'),
            ('US', 'CA', 'California'),
            ('US', 'AK', 'Alaska'),
            ('US', 'KS', 'Kansas');

INSERT INTO cidade(nome, uf, pais)
     VALUES ('La Plata', 'BA', 'AR'),
            ('Joao Pessoa', 'PB', 'BR'),
            ('Patos', 'PB', 'BR'),
            ('Campina Grande', 'PB', 'BR'),
            ('Sape', 'PB', 'BR'),
            ('Recife', 'PE', 'BR'),
            ('Buenos Aires', 'BA', 'AR'),
            ('Rio Branco', 'AC', 'BR'),
            ('Cruzeiro do Sul', 'AC', 'BR'),
            ('Porto Acre', 'AC', 'BR'),
            ('Sao Paulo', 'SP', 'BR'),
            ('Santa Rita', 'PB', 'BR'),
            ('Guarabira', 'PB', 'BR'),
            ('Santos', 'SP', 'BR'),
            ('Los Angeles', 'CA', 'US'),
            ('Anchorage', 'AK', 'US'),
            ('Topeka', 'KS', 'US');

INSERT INTO cliente (cgc_cpf, tipo, razao_social, tipo_logradouro, logradouro, complemento,
         bairro, cidade, cep, fone, contato, email)
VALUES
('102.429.288-26','F','Daniela Garçon','AVE','Epitacio Pessoa','N 666, APT 606','Sao Jose',2,58000001,'8332315044','8399123123','danigarçon@gmail.com'),
('304.419.488-64','F','Ciro Gomes','RUA','Ulysses Guimaraes','N 12','Gramame',6,58000001,'8433428754','8499900123','cirogomes@gmail.com'),
('102.102.102-02','F','Luis Silva','AVE','Maria do Ceu','N 13','Bairro Sul',11,57300001,'1148565123','1191234123','lulapt@gmail.com'),
('102.121.748-33','F','Galvao Bueno','AVE','Pidgeot','N 7','Bairro Norte',11,56400001,'1141215044','1188880123','falomuito@gmail.com'),
('24098477000110','J','UFPB','AVE','Beira Rio','n 45, Campus I','Castelo Branco',2,58000002,'8348654334','8390000003','ufpb@gmail.com');



SELECT * FROM cliente;

INSERT INTO tipo_produto(nome)
VALUES ('Parachoque'),
       ('Janela traseira'),
	   ('Janela frontal'),
	   ('Volante'),
	   ('Pneu'),
	   ('Roda'),
	   ('Cabo de vela');

INSERT INTO produto(nome, tipo, unidade, estoque_real, estoque_virtual, estoque_minimo, custo, venda)
     VALUES ('Parachoque Fiat Uno 2004', 1, 'und', 13, 10, 5, 100, 900),
            ('Parachoque Fiat UNO 2016', 1, 'und', 130, 110, 5, 200, 1100),
            ('Parachoque Fiesta 2015', 1, 'und', 125, 85, 5, 200, 1000),
            ('Pneu Aro 13', 5, 'und', 35, 32, 15, 200, 250),
            ('Pneu Aro 14', 5, 'und', 30, 30, 15, 180, 300),
            ('Pneu Aro 15', 5, 'und', 40, 33, 15, 190, 350);


INSERT INTO funcionario(nome, sexo, setor, ramal_individual, estado_civil, data_nascimento, rg_numero,
                        naturalidade, cpf, sangue_fator, sangue_rh, tipo_logradouro, logradouro,
                        complemento, bairro, cidade, cep, fone, funcao, admissao, email, salario)
     VALUES ('Silvino Silvano Silveira', 'M', 1, '0800', 'S', '1964-04-18 10:34:09',
            '6258003', 3, '78945612538', 'AB', '+', 'RUA', 'Rua das Moitas Ardentes',
            'Casa', 'Centro', 4, '58200000', '08002148965237', 2, '2015-06-18 10:34:09',
            'cobra@ig.com',3500),

            ('Cristiane Bloco das Virgens', 'F',2,'5863','C','1983-06-18 10:34:09','6258600',
            2,'98675342805','A','-','AVE', 'Av. Pedro Cristina Motta','Apartamento Sagaz',
            'Florentina de Jesus',3,'10867642','85741968500234',1,'2014-06-18 10:34:09',
		    'vigimainha@mobil.net',7812),

    		('Falácio Grande da Silva','M', 3, '6912','C', '1992-06-18 10:34:09','1248600',
            1, '98673812805', 'O', '-','RUA', 'Rua Joventina Jovêncio Jovem', 'Casa','Cristo',
            2,'62786004','8395741285',3,'2012-06-18 10:34:09','oxente@nex.com',15236 ),

		    ('Marcelo Teixeira','M', 3, '6911','S', '1991-11-17 10:34:09','1248601',12,
		    '12373814505', 'A', '+','AVE', 'Av Laura Costa', 'Casa','Bairro Santo',12,
		    '62786098','8395796385',3,'2012-06-18 10:34:09','marcelo@nex.com',888 ),

		    ('Fernanda Marques','M', 3, '6910','C', '1989-08-13 10:34:09','1248602',13,
		    '95678812875', 'O', '-','RUA', 'Rua Louca da Silva', 'Casa','Bairro da Paz',13,
		    '62746804','8396661285',3,'2012-06-18 10:34:09','fernanda@nex.com',8136 ),

		    ('Marcela Leite','F', 2, '6909','S', '1991-11-17 10:34:09','121',12,
		    '13333814505', 'A', '+','AVE', 'Av Laura Costa', 'Casa','Bairro Santo',12,
		    '62786098','8395796385',3,'2012-06-18 10:34:09','marcela@nex.com',1000 ),

		    ('Fernandinho Beira Mar','M', 2, '6908','D', '1970-04-13 10:34:09','122',13,
		    '97778812875', 'O', '+','RUA', 'Rua Louca da Silva', 'Casa','Bairro da Paz',13,
		    '62746804','8396661285',3,'2012-06-18 10:34:09','fernandinho@nex.com',1200 ),

		    ('Marceleto Lopes','M', 2, '6907','D', '1999-04-17 10:34:09','123',12,
		    '15553814505', 'A', '-','AVE', 'Av Laura Costa', 'Casa','Bairro Santo',12,
		    '62786098','8395796385',3,'2012-06-18 10:34:09','marceleto@nex.com',2000 ),

		    ('Jaqueline Sousa','F', 3, '6906','C', '1945-05-13 10:34:09','124',13,
		    '94448812875', 'O', '-','RUA', 'Rua Louca da Silva', 'Casa','Bairro da Paz',13,
		    '62746804','8396661285',3,'2012-06-18 10:34:09','jaq@nex.com',6000 );

INSERT INTO pedido(cliente, data_pedido, total, situacao, vendedor)
     VALUES (5, '2016-02-18 10:34:09', 358.26, 'A', 7),
            (2, '2016-01-18 10:34:09', 698.26, 'T', 1),
            (3, '2015-06-18 10:34:09', 684.64, 'P', 1),
            (4, '2016-04-01 10:34:09', 208.69, 'T', 4),
            (1, '2016-06-01 10:34:09', 587.36, 'A', 2),
            (2, '2016-01-23 10:34:09', 602.26, 'T', 4),
            (3, '2015-06-22 10:34:09', 896.64, 'P', 5),
            (4, '2016-04-19 10:34:09', 226.69, 'T', 2),
            (2, '2015-06-18 10:34:09', 1000.00, 'A', 3),
            (3, '2014-07-25 17:23:54', 100.00, 'T', 3),
            (4, '2016-01-13 19:23:52', 200.00, 'P', 9),
            (5, '2013-12-10 07:21:35', 100.00, 'P', 7);

INSERT INTO itens_pedido(pedido_id, produto_id, quant, total)
     VALUES (1, 2, 258.26, 234.26),
            (2, 1, 328.26, 3314.68),
            (3, 4, 12.458, 4545.56),
            (4, 2, 2554.36, 454.151),
            (1, 3, 125.56, 1251.125),
            (4, 3, 5151.125, 451.1521),
            (1, 5, 14, 243.23),
            (2, 6, 190, 432.5),
            (3, 6, 20, 123.4),
            (4, 1, 10, 1222.43),
            (5, 4, 100, 122223.4),
            (5, 6, 100, 66653.22);

INSERT INTO setor(nome, sigla, ramal, superior, responsavel)
     VALUES 
     		('Area de Lazer1', 'LAZER1', '191', 1, 1),
     		('Area de Lazer2', 'LAZER2', '192', 2, 1),
     		('Area de Lazer3', 'LAZER', '193', 1, 1),
     		('Area de Lazer4', 'LAZER', '194', 3, 5),
     		('Area de Lazer5', 'LAZER', '195', 2, 4),
     		('Area de Lazer6', 'LAZER', '196', 1, 1),
     		('Area de Lazer7', 'LAZER', '197', 1, 2);
            