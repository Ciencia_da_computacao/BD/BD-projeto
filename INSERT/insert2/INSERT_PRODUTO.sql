USE mydb2;

INSERT INTO produto(nome, tipo, unidade, estoque_real, estoque_virtual, estoque_minimo, custo, venda)
     VALUES ('Parachoque Fiat Uno 2004', 1, 'und', 13, 10, 5, 100, 900),
            ('Parachoque Fiat UNO 2016', 1, 'und', 130, 110, 5, 200, 1100),
            ('Parachoque Fiesta 2015', 1, 'und', 125, 85, 5, 200, 1000),
            ('Pneu Aro 13', 5, 'und', 35, 32, 15, 200, 250),
            ('Pneu Aro 14', 5, 'und', 30, 30, 15, 180, 300),
            ('Pneu Aro 15', 5, 'und', 40, 33, 15, 190, 350);

/*SELECT *FROM PRODUTO;*/