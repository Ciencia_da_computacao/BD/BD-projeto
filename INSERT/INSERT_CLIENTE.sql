USE mydb;

INSERT INTO cliente (cgc_cpf, tipo, razao_social, tipo_logradouro, logradouro, complemento,
                     bairro, cidade, cep, fone, contato, email)
VALUES
('102.429.288-26','F','Daniela Garçon','AVE','Epitacio Pessoa','N 666, APT 606','Sao Jose',2,58000001,'8332315044','8399123123','danigarçon@gmail.com'),
('304.419.488-64','F','Ciro Gomes','RUA','Ulysses Guimaraes','N 12','Gramame',6,58000001,'8433428754','8499900123','cirogomes@gmail.com'),
('102.102.102-02','F','Luis Silva','AVE','Maria do Ceu','N 13','Bairro Sul',11,57300001,'1148565123','1191234123','lulapt@gmail.com'),
('102.121.748-33','F','Galvao Bueno','AVE','Pidgeot','N 7','Bairro Norte',11,56400001,'1141215044','1188880123','falomuito@gmail.com'),
('24098477000110','J','UFPB','AVE','Beira Rio','n 45, Campus I','Castelo Branco',2,58000002,'8348654334','8390000003','ufpb@gmail.com');



SELECT * FROM cliente;