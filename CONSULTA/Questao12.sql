
SELECT AVG(FLOOR(DATEDIFF(DAY, f.data_nascimento, getdate()) / 365.25)) as media_idade
  FROM funcionario as f
 WHERE f.sexo = 'M'
   AND f.funcao = (

    SELECT cont_func.funcao
      FROM
    (
          SELECT funcao.codigo_id as funcao, COUNT(f.funcao) as funcionarios_por_funcao
            FROM funcionario as f, funcao
           WHERE f.funcao = funcao.codigo_id
             AND f.sexo = 'M'
        GROUP BY funcao.codigo_id
    ) cont_func
     WHERE cont_func.funcionarios_por_funcao = (SELECT MAX(cf.funcionarios_por_funcao) 
                                                  FROM
                                               (
                                                    SELECT COUNT(f.funcao) as funcionarios_por_funcao
                                                      FROM funcionario as f, funcao
                                                     WHERE f.funcao = funcao.codigo_id
                                                       AND f.sexo = 'M'
                                                  GROUP BY funcao.codigo_id
                                                    ) cf
                                               )
)