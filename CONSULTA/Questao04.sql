SELECT AVG(FLOOR(DATEDIFF(DAY, data_nascimento, getdate()) / 365.25)) as media_idade
  FROM funcionario
 WHERE sexo = 'F'
   AND setor > 1
   AND setor < 4
   AND funcao = 1;
