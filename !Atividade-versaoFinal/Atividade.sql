/*


Projeto MySQL - Banco de Dados I 
Profesor Alan Moraes
Alunos:
	Luiz Henrique - 11514334
	Alisson Galiza - 11408126
	Aline Moura - 11512963



*/
-- O Script a seguir trata-se do banco normalizado da 1FN à 3FN. 

-- Criação das Tabelas 


-- Criação do Banco 

-- drop database mydb; 
create database mydb; 
use mydb; 

-- Criação das Tabelas 

-- funcao
CREATE TABLE funcao
(
    codigo_id INT      AUTO_INCREMENT,
    nome      CHAR(50) NULL,
    salario   NUMERIC(16,2)    NULL,

    CONSTRAINT pk_funcao_codigo PRIMARY KEY(codigo_id)
);

-- pais
CREATE TABLE pais
(
    sigla_id CHAR(2)  NOT NULL,
    nome     CHAR(40) NULL,

    CONSTRAINT pk_pais_sigla PRIMARY KEY(sigla_id)
);


-- uf
CREATE TABLE uf
(
    sigla_id CHAR(2)  NOT NULL,
    pais_id  CHAR(2)  NOT NULL,
    nome     CHAR(30) NOT NULL,

    CONSTRAINT pk_uf_sigla PRIMARY KEY(sigla_id, pais_id),
    CONSTRAINT fk_uf_pais  FOREIGN KEY(pais_id) REFERENCES pais(sigla_id)
);
-- cidade
CREATE TABLE cidade
(
    codigo_id INT      AUTO_INCREMENT,
    nome      CHAR(40) NULL,
    uf        CHAR(2)  NOT NULL,
    pais      CHAR(2)  NOT NULL,

    CONSTRAINT pk_cidade_codigo PRIMARY KEY(codigo_id),
    CONSTRAINT fk_cidade_uf     FOREIGN KEY(uf, pais) REFERENCES uf(sigla_id, pais_id),
    CONSTRAINT fk_cidade_pais   FOREIGN KEY(pais) REFERENCES pais(sigla_id)
);

-- cliente
CREATE TABLE cliente
(
    codigo_id       SMALLINT AUTO_INCREMENT,
    cgc_cpf         CHAR(15)               NOT NULL,
    tipo            CHAR(1)                NOT NULL
        CHECK (tipo = 'F' or tipo = 'J'),
    razao_social    CHAR(30)               NOT NULL,
    tipo_logradouro CHAR(3)                NOT NULL
        CHECK (tipo_logradouro = 'AVE' OR tipo_logradouro = 'RUA' OR tipo_logradouro = 'PRA' OR tipo_logradouro = 'TRA'
               OR tipo_logradouro = 'TRA' OR tipo_logradouro = 'ROD' OR tipo_logradouro = 'VIL'),
    logradouro      CHAR(30)               NOT NULL,
    complemento     CHAR(20)               NULL,
    bairro          CHAR(20)               NOT NULL,
    cidade          INT                    NOT NULL,
    cep             CHAR(8)                NOT NULL,
    fone            CHAR(14)               NOT NULL,
    contato         CHAR(30)               NOT NULL,
    fax             CHAR(14)               NULL,
    insc_est        CHAR(16)               NULL,
    email           CHAR(40)               NULL,
    obs             TEXT                   NULL,

    CONSTRAINT pk_cliente_codigo PRIMARY KEY(codigo_id),
    CONSTRAINT cgc_cpf_unique    UNIQUE(cgc_cpf),
    CONSTRAINT fk_cliente_cidade FOREIGN KEY(cidade)   REFERENCES cidade(codigo_id)
);

-- tipo_produto
CREATE TABLE tipo_produto
(
    codigo_id SMALLINT AUTO_INCREMENT,
    nome      CHAR(50) NULL,

    CONSTRAINT pk_tipo_produto_codigo PRIMARY KEY(codigo_id)
);

-- produto
CREATE TABLE produto
(
    codigo_id       SMALLINT              AUTO_INCREMENT,
    nome            CHAR(40)              NOT NULL,
    tipo            SMALLINT              NOT NULL,
    unidade         CHAR(3)               NOT NULL,
    estoque_real    NUMERIC(16,3)         NULL,
    estoque_virtual NUMERIC(16,3)         NULL,
    estoque_minimo  NUMERIC(16,3)         NULL,
    custo           NUMERIC(16,2)         NULL,
    venda           NUMERIC(16,2)         NULL,

    CONSTRAINT pk_produto_codigo PRIMARY KEY(codigo_id),
    CONSTRAINT fk_produto_tipo   FOREIGN KEY(tipo) REFERENCES tipo_produto(codigo_id)
);

-- funcionario
CREATE TABLE funcionario
(
    codigo_id        INT           AUTO_INCREMENT,
    nome             CHAR(30)      NOT NULL,
    sexo             CHAR(1)       NOT NULL
        CHECK (sexo = 'M' OR sexo = 'F'),
    setor            SMALLINT      NULL,
    ramal_individual CHAR(4)       NOT NULL,
    estado_civil     CHAR(1)       NOT NULL
        CHECK (estado_civil = 'S' OR estado_civil = 'C' OR estado_civil = 'D'
               OR estado_civil = 'V' OR estado_civil = 'O'),
    data_nascimento  DATETIME NOT NULL,
    rg_numero        CHAR(15)      NULL,
    naturalidade     INT           NOT NULL,
    cpf              CHAR(11)      NULL,
    sangue_fator     CHAR(2)       NULL,
    sangue_rh        CHAR(1)       NULL,
    tipo_logradouro  CHAR(3)       NULL
        CHECK (tipo_logradouro = 'AVE' OR tipo_logradouro = 'RUA' OR tipo_logradouro = 'PRA' OR tipo_logradouro = 'TRA'
               OR tipo_logradouro = 'TRA' OR tipo_logradouro = 'ROD' OR tipo_logradouro = 'VIL'),
    logradouro       CHAR(30)      NOT NULL,
    complemento      CHAR(30)      NULL,
    bairro           CHAR(20)      NOT NULL,
    cidade           INT           NOT NULL,
    cep              CHAR(8)       NULL,
    fone             CHAR(14)      NULL,
    funcao           INT           NOT NULL,
    admissao         DATETIME      NOT NULL,
    email            CHAR(40)      NULL,
    salario          NUMERIC(16,2)         NOT NULL,

    CONSTRAINT pk_funcionario_codigo       PRIMARY KEY(codigo_id),
    CONSTRAINT fk_funcionario_naturalidade FOREIGN KEY(naturalidade) REFERENCES cidade(codigo_id),
    CONSTRAINT fk_funcionario_cidade       FOREIGN KEY(cidade)       REFERENCES cidade(codigo_id),
    CONSTRAINT fk_funcionario_funcao       FOREIGN KEY(funcao)       REFERENCES funcao(codigo_id),
	-- CONSTRAINT fk_funcionario_setor		   FOREIGN KEY (setor) 		 REFERENCES setor(codigo_id),
    CONSTRAINT cpf_unique                  UNIQUE(cpf),
    CONSTRAINT rg_unique                   UNIQUE(rg_numero)
);

-- pedido
CREATE TABLE pedido
(
    codigo_id   INT           AUTO_INCREMENT,
    cliente     SMALLINT      NOT NULL,
    data_pedido DATETIME NOT NULL,
    total       NUMERIC(16,2)         NULL,
    situacao    CHAR(1)       NULL
        CHECK (situacao = 'A' OR situacao = 'P' OR situacao = 'T'),
    vendedor    INT           NULL,

    CONSTRAINT pk_pedido_codigo   PRIMARY KEY(codigo_id),
    CONSTRAINT fk_pedido_cliente  FOREIGN KEY(cliente)  REFERENCES cliente(codigo_id),
    CONSTRAINT fk_pedido_vendedor FOREIGN KEY(vendedor) REFERENCES funcionario(codigo_id)
);


-- setor
CREATE TABLE setor
(
    codigo_id   SMALLINT AUTO_INCREMENT,
    nome        CHAR(50) NULL,
    sigla       CHAR(10) NULL,
    ramal       CHAR(3)  NULL,
    superior    SMALLINT NULL,
    responsavel INT      NULL,

    CONSTRAINT pk_setor_codigo      PRIMARY KEY(codigo_id),
    CONSTRAINT fk_setor_responsavel FOREIGN KEY(responsavel) REFERENCES funcionario(codigo_id),
    CONSTRAINT fk_setor_superior    FOREIGN KEY(superior) REFERENCES setor(codigo_id)
);

-- intens_pedido
CREATE TABLE itens_pedido
(
    pedido_id  INT           NOT NULL,
    produto_id SMALLINT      NOT NULL,
    quant      NUMERIC(10,3) NOT NULL,
    total      NUMERIC(16,2) NULL,
    situacao   CHAR(1)       NULL
        CHECK (situacao = 'A' OR situacao = 'P' OR situacao = 'T'),

    CONSTRAINT pk_itens_pedidos         PRIMARY KEY(pedido_id, produto_id),
    CONSTRAINT fk_itens_pedido_pedido  FOREIGN KEY(pedido_id)  REFERENCES pedido(codigo_id),
    CONSTRAINT fk_itens_pedido_produto FOREIGN KEY(produto_id) REFERENCES produto(codigo_id)
);

-- A ORDEM DE INSERÇÃO INICIAL DEVIDO À NORMALIZAÇÃO SE DÁ
-- DA SEGUINTE MANEIRA.

-- |FUNÇÃO|PAÍS|UF|CIDADE|CLIENTE|TIPO_PRODUTO|PRODUTO|
-- |FUNCIONARIO|PEDIDO|SETOR|ITENS_PEDIDO|

-- DEMAIS INSERÇÕES NÃO PRECISAM SEGUIR TAL SEQUÊNCIA

-- POPULAR TABELAS


INSERT INTO funcao(nome, salario)
     VALUES ('Gerente de Engenharia', 2700.12),
            ('Gerente de Qualidade', 13678.25),
            ('Gerente de Vendas', 14439.00),
            ('Gerente de Logistica', 13110.86),
            ('Coordenador de Sistema de Qualidade', 6557.00),
            ('Gerente de Producao', 13079.00),
            ('Engenheiro de Manutenção', 9043.00),
            ('Médico do Trabalho', 7500.00),
            ('Coordenador de Processos', 9241.00),
            ('Engenheiro de Desenvolvimento', 6749.00),
            ('Analista de Controle de Qualidade', 6942.00);

INSERT INTO pais(sigla_id, nome)
     VALUES ('BR', 'Brasil'),
            ('PT', 'Portugal'),
            ('US', 'Estados Unidos'),
            ('AR', 'Argentina'),
            ('FR', 'França'),
            ('CA', 'Canada');

INSERT INTO uf(pais_id, sigla_id, nome)
     VALUES ('BR', 'PB', 'Paraiba'),
            ('BR', 'AM', 'Amazonas'), 
            ('BR', 'PA', 'Pará'),
            ('AR', 'SF', 'Santa Fe'),
            ('BR', 'RS', 'Rio Grande do Sul'),
            ('BR', 'AC', 'Acre'),
            ('BR', 'RN', 'Rio Grande do Norte'),
            ('BR', 'SP', 'Sao Paulo'),
            ('US', 'AK', 'Alaska'),
            ('AR', 'BA', 'Provincia de Buenos Aires'),
            ('AR', 'SL', 'San Luis'),
            ('BR', 'PE', 'Pernambuco'),
            ('US', 'CA', 'California'),
            ('US', 'KS', 'Kansas');

INSERT INTO cidade(nome, uf, pais)
     VALUES ('La Plata', 'BA', 'AR'),
            ('Patos', 'PB', 'BR'),
            ('Sape', 'PB', 'BR'),
            ('Joao Pessoa', 'PB', 'BR'),
            ('Campina Grande', 'PB', 'BR'),
            ('Belém', 'PA', 'BR'),
            ('Manaus','AM', 'BR'),
            ('Recife', 'PE', 'BR'),
            ('Cruzeiro do Sul', 'AC', 'BR'),
            ('Santa Rita', 'PB', 'BR'),
            ('Rio Branco', 'AC', 'BR'),
            ('Buenos Aires', 'BA', 'AR'),
            ('Porto Acre', 'AC', 'BR'),
            ('Sao Paulo', 'SP', 'BR'),
            ('Guarabira', 'PB', 'BR'),
            ('Santos', 'SP', 'BR'),
            ('Anchorage', 'AK', 'US'),
            ('Los Angeles', 'CA', 'US'),
            ('Topeka', 'KS', 'US');

INSERT INTO cliente (cgc_cpf, tipo, razao_social, tipo_logradouro, logradouro, complemento,bairro, cidade, cep, fone, contato, email)
VALUES  ('102.429.288-26','F','Daniela Garçon','AVE','Epitacio Pessoa','N 666, APT 606','Sao Jose',2,58000001,'8332315044','8399123123','danigarçon@gmail.com'),
	    ('304.419.488-64','F','Ciro Gomes','RUA','Ulysses Guimaraes','N 12','Gramame',6,58000001,'8433428754','8499900123','cirogomes@gmail.com'),
        ('102.102.102-02','F','Luis Silva','AVE','Maria do Ceu','N 13','Bairro Sul',11,57300001,'1148565123','1191234123','lulapt@gmail.com'),
        ('102.121.748-33','F','Galvao Bueno','AVE','Pidgeot','N 7','Bairro Norte',11, 56400001,'1141215044','1188880123','falomuito@gmail.com'),
        ('24098477000110','J','UFPB','AVE','Beira Rio','n 45, Campus I','Castelo Branco', 2, 58888001, '8348654334','8390000003','ufpb@gmail.com'),
		('412.123.432.54','F','Alana Santos','AVE', 'Piaui, n 72', 'CASA', 'ESTADOS', 2, 58000190, '8330331276', '83303312122','alana@gmail.com');

INSERT INTO tipo_produto(nome)
VALUES ('Parachoque'),
	   ('Volante'),
       ('Janela Fronteira'),
       ('Janela Traseira'),
       ('Pneu '),
       ('Cabo de vela'), 
       ('Limpador de Parabrisa'), 
       ('Roda'),
	   ('Luz Interna Painel Frontal'); 

INSERT INTO produto(nome, tipo, unidade, estoque_real, estoque_virtual, estoque_minimo, custo, venda)
     VALUES ('Parachoque Fiat Uno 2004', 1, 'und', 13, 10, 5, 100, 900),
            ('Parachoque Fiat UNO 2016', 1, 'und', 130, 110, 5, 200, 1100),
            ('Pneu Aro 13', 5, 'und', 35, 32, 15, 200, 250),
            ('Parachoque Fiesta 2015', 1, 'und', 125, 85, 5, 200, 1000),
            ('Pneu Aro 14', 5, 'und', 30, 30, 15, 180, 300),
            ('Pneu Aro 15', 5, 'und', 40, 33, 15, 190, 350);


INSERT INTO funcionario(nome, sexo, setor, ramal_individual, estado_civil, data_nascimento, rg_numero,
                        naturalidade, cpf, sangue_fator, sangue_rh, tipo_logradouro, logradouro,
                        complemento, bairro, cidade, cep, fone, funcao, admissao, email, salario)
                        
     VALUES ('Silvino Silvano Silveira', 'M', 1, '0800', 'S', '1964-04-18 10:34:09',
            '6258003', 3, '78945612538', 'AB', '+', 'RUA', 'Rua das Moitas Ardentes',
            'Casa', 'Centro', 4, '58200000', '08002148965237', 2, '2016-06-18 10:34:09',
            'cobra@ig.com',3500),

            ('Cristiane Bloco das Virgens', 'F',2,'5863','C','1983-06-18 10:34:09','6258600',
            2,'98675342805','A','-','AVE', 'Av. Pedro Cristina Motta','Apartamento Sagaz',
            'Florentina de Jesus',3,'10867642','85741968500234',1,'2016-06-18 10:34:09',
		    'vigimainha@mobil.net',7812),

            ('Falácio Grande da Silva','M', 2, '6912','C', '1992-06-18 10:34:09','1248600',
            1, '98673812805', 'O', '-','RUA', 'Rua Joventina Jovêncio Jovem', 'Casa','Cristo',
            2,'62786004','8395741285',3,'2012-06-18 10:34:09','oxente@nex.com',15236 ),

		    ('Marcelo Teixeira','M', 2, '6911','S', '1991-11-17 10:34:09','1248601',12,
		    '12373814505', 'A', '+','AVE', 'Av Laura Costa', 'Casa','Bairro Santo',12,
		    '62786098','8395796385',3,'2012-06-18 10:34:09','marcelo@nex.com',888 ),

		    ('Fernanda Marques','M', 1, '6910','C', '1989-08-13 10:34:09','1248602',13,
		    '95678812875', 'O', '-','RUA', 'Rua Louca da Silva', 'Casa','Bairro da Paz',13,
		    '62746804','8396661285',3,'2016-06-18 10:34:09','fernanda@nex.com',8136 ),

		    ('Marcela Leite','F', 2, '6909','S', '1991-11-17 10:34:09','121',12,
		    '13333814505', 'A', '+','AVE', 'Av Laura Costa', 'Casa','Bairro Santo',12,
		    '62786098','8395796385',3,'2012-06-18 10:34:09','marcela@nex.com',1000 ),

		    ('Fernandinho Beira Mar','M', 2, '6908','D', '1970-04-13 10:34:09','122',13,
		    '97778812875', 'O', '+','RUA', 'Rua Louca da Silva', 'Casa','Bairro da Paz',13,
		    '62746804','8396661285',3,'2012-06-18 10:34:09','fernandinho@nex.com',1200 ),

		    ('Marceleto Lopes','M', 2, '6907','D', '1999-04-17 10:34:09','123',12,
		    '15553814505', 'A', '-','AVE', 'Av Laura Costa', 'Casa','Bairro Santo',12,
		    '62786098','8395796385',3,'2012-06-18 10:34:09','marceleto@nex.com',2000 ),

		    ('Jaqueline Sousa','F', 3, '6906','C', '1945-05-13 10:34:09','124',13,
		    '94448812875', 'O', '-','RUA', 'Rua Louca da Silva', 'Casa','Bairro da Paz',13,
		    '62746804','8396661285',3,'2012-06-18 10:34:09','jaq@nex.com',6000 );

INSERT INTO pedido(cliente, data_pedido, total, situacao, vendedor)
     VALUES (5, '2016-02-18 10:34:09', 358.26, 'A', 7),
            (2, '2016-01-23 10:34:09', 602.26, 'T', 4),
            (1, '2016-06-01 10:34:09', 587.36, 'A', 2),
            (4, '2016-04-19 10:34:09', 226.69, 'T', 2),
            (4, '2016-04-01 10:34:09', 208.69, 'T', 4),
            (3, '2014-07-25 17:23:54', 100.00, 'T', 3),
            (3, '2015-06-22 10:34:09', 896.64, 'P', 5),
            (2, '2016-01-18 10:34:09', 698.26, 'T', 1),
            (3, '2015-06-18 10:34:09', 684.64, 'P', 1),
            (4, '2016-01-13 19:23:52', 200.00, 'P', 9),
            (2, '2015-06-18 10:34:09', 1000.00,'A', 3),
            (5, '2013-12-10 07:21:35', 100.00, 'P', 7);

INSERT INTO setor(codigo_id, nome, sigla, ramal, superior, responsavel)
     VALUES (1,'Recursos Humanos', 'RH', '190', NULL, 1),
     		(2,'Gerência de Projetos','GP', '191', 1, 2),
     		(3, 'Controle de Qualidade', 'CQ', '192', 1, 3);

INSERT INTO itens_pedido(pedido_id, produto_id, quant, total)
     VALUES (1, 2, 258.26, 234.26),
            (2, 1, 328.26, 3314.68),
            (3, 4, 12.458, 4545.56),
            (4, 2, 2554.36, 454.151),
            (1, 3, 125.56, 1251.125),
            (4, 3, 5151.125, 451.1521),
            (1, 5, 14, 243.23),
            (2, 6, 190, 432.5),
            (3, 6, 20, 123.4),
            (4, 1, 10, 1222.43),
            (5, 4, 100, 122223.4),
            (5, 6, 100, 66653.22);


-- QUESTÕES RESOLVIDAS
-- Escolhemos resolver as ímpares

-- Questão 1) 
use mydb;
SELECT * FROM funcionario;
SELECT * FROM setor;
SELECT * FROM funcao;
SELECT * FROM cidade;
SELECT * FROM uf;
SELECT * FROM pais;
SELECT * FROM produto;
SELECT * FROM tipo_produto;
SELECT * FROM pedido;
SELECT * FROM itens_pedido;
SELECT * FROM cliente;

-- Questão 3) 
use mydb;

SELECT cliente.cidade FROM cliente;
SELECT DISTINCT cliente.cidade FROM cliente;

-- Questão 5) 
-- colocar os valores
SELECT * FROM produto;
SELECT produto.venda * 1.35 AS 'AUMENTO' FROM produto WHERE produto.codigo_id > 0;

-- Questão 7) 
SELECT funcionario.nome, funcionario.setor FROM mydb.funcionario
WHERE cidade = 13 or cidade = 12
ORDER BY setor, nome;

-- Questão 9) 
SELECT uf FROM cidade WHERE pais != 'BR'
ORDER BY uf desc;

-- Questão 11) 

SELECT nome, salario, salario * 1.2 AS 'Aumento' FROM funcionario WHERE codigo_id>0 AND setor != 10 AND sexo = 'F';

-- Questão 13) 
SELECT * FROM produto WHERE venda < 100;

-- Questão 15) 
SELECT nome FROM funcionario WHERE sexo = 'M'
AND nome REGEXP '[^ABC]'
AND YEAR(funcionario.admissao) = YEAR(current_date())-1;

-- Questão 17) 

SELECT nome, venda FROM produto WHERE nome LIKE  '%freio%' ; 

-- Questão 19) 

SELECT razao_social,contato, email FROM cliente WHERE email IS NOT NULL;

-- Questão 21) 

-- inserindo um  valor que corresponde à busca
INSERT INTO mydb.funcionario (nome, sexo, setor, ramal_individual, estado_civil, data_nascimento, rg_numero,
                        naturalidade, cpf, sangue_fator, sangue_rh, tipo_logradouro, logradouro,
                        complemento, bairro, cidade, cep, fone, funcao, admissao, email, salario)
VALUES 		('Daniel Barros', 'M', 1, '0800', 'S', '1966-01-18 10:34:09',
            '----sd---', 3, '09414263475', 'AB', '+', 'RUA', 'Rua das Roseiras',
            'Casa', 'Centro', 4, '58200000', '08002148965237', 2, '2015-06-18 10:34:09',
            'cobra@ig.com',3500);

-- busca
SELECT * FROM funcionario WHERE data_nascimento BETWEEN
'1965-12-01 00:00:00' AND '1966-02-28 23:59:59';

-- Questão 23
SELECT SUM(salario) AS 'Soma dos salarios' FROM funcao;

-- Questão 25) 

-- como não temos setor 8, 
INSERT INTO `funcionario` 
		(`codigo_id`, `nome`, `sexo`, `setor`, `ramal_individual`, `estado_civil`, `data_nascimento`, `rg_numero`, `naturalidade`, `cpf`, `sangue_fator`, `sangue_rh`, `tipo_logradouro`, `logradouro`, `complemento`, `bairro`, `cidade`, `cep`, `fone`, `funcao`, `admissao`, `email`, `salario`) 
VALUES  (NULL, 'Fernanda Ribeiros', 'F', '8', '234', 'C', '1996-11-28 00:00:00', '6555123', '16', '00099912334', 'A', 'P', 'RUA', 'ANJOS DO JARDIM', 'ABC', 'LURDES', '3', '87906543', '78765656765456', '3', '2017-11-14 00:00:00', 'fernanda@gmail.com', '1000.27'), 
		(NULL, 'JOSILENE', 'F', '8', '133', 'C', '1997-11-21 00:00:00', '5554123', '4', '12312455123', 'AB', '+', 'RUA', 'ANCHIETA MARIA TORRES', 'CASA', 'SAO BRAZ', '4', '55331234', '55555555', '4', '2017-11-14 00:00:00', 'josi@gmail.com', '905.72');

SELECT SUM(salario)/COUNT(setor) FROM funcionario WHERE setor = 8;

-- Questão 27) 

SELECT SUM(year(current_date()) - year(data_nascimento))/COUNT(codigo_id) FROM funcionario WHERE funcao = 2 and sexo = 'M'; 

-- Questão 29) 

SELECT nome, admissao FROM funcionario ORDER BY admissao ASC LIMIT 1 ; 

-- Questão 31) 

SELECT COUNT(ramal_individual) AS 'Ramal' FROM funcionario WHERE ramal_individual != NULL; 

-- Questão 33) 

SELECT MAX(venda) FROM produto;

-- Questão 35)

SELECT * FROM pedido ORDER BY data_pedido DESC LIMIT 1; 

-- Questão 37)

SELECT SUM(salario) AS 'Total de salario', setor FROM funcionario
GROUP BY setor;

-- Questão 39)

SELECT AVG(venda), tipo FROM produto
GROUP BY (tipo); 

-- Questão 41)

SELECT produto_id,COUNT(*) AS 'Quantidade de pedidos',
	AVG(total) AS 'Media do preco', 
	AVG(quant) AS 'Media de quant'
FROM itens_pedido
GROUP BY produto_id;

-- Questão 43) 

SELECT SUM(total), cliente FROM pedido 
WHERE year(data_pedido) = year(current_date()) - 1
GROUP BY cliente;

-- Questão 45) 
SELECT sexo, setor, COUNT(codigo_id) AS 'Quantidade'
FROM funcionario
GROUP BY sexo, setor;

-- Questão 47) 
SELECT COUNT(codigo_id) AS 'contador',
	sexo, naturalidade
FROM funcionario
GROUP BY sexo, naturalidade;

-- Questão 49)

SELECT pais, COUNT(*) AS ' Por País ' FROM cidade 
GROUP BY (pais);

-- Questão 51)

SELECT COUNT(codigo_id) AS 'Funcionarios', cidade, sexo
FROM funcionario
GROUP BY cidade, sexo; 
 
-- Questão 53)

SELECT funcionario.nome,
	   setor.nome
FROM funcionario
INNER JOIN setor ON funcionario.setor=setor.codigo_id;

-- Questão 55)

SELECT cliente.razao_social, cidade.nome 
FROM cliente INNER JOIN cidade 
ON cliente.cidade = cidade.codigo_id;

-- Questão 57)

SELECT  produto.codigo_id AS 'Codigo do produto',
		produto.venda AS 'Preço de venda',
   		produto.estoque_real AS 'Estoque real',
    	produto.tipo AS 'Código do tipo',
   		tipo_produto.nome AS 'Nome do tipo'
FROM produto
INNER JOIN tipo_produto ON
	produto.tipo = tipo_produto.codigo_id;

-- Questão 59)

SELECT SUM(funcionario.salario), setor.codigo_id, setor.nome
FROM funcionario INNER JOIN setor
ON funcionario.setor = setor.codigo_id
GROUP BY (setor.codigo_id);

-- Questão 61)

SELECT setor1.nome AS 'Nome do filho', setor2.nome AS 'Nome do superior'
FROM setor AS setor1
INNER JOIN setor AS setor2
ON setor2.codigo_id = setor1.superior;

-- Questão 63)

SELECT pedido.codigo_id as 'Codigo do Pedido' , cliente.razao_social as 'Nome do Cliente',
pedido.situacao as 'Status do Pedido' FROM pedido 
INNER JOIN cliente on pedido.cliente = cliente.codigo_id 
WHERE pedido.situacao;

-- Questão 65)

SELECT  cliente.razao_social, pais.nome
FROM cliente
LEFT JOIN cidade ON cliente.cidade = cidade.codigo_id
LEFT JOIN pais ON cidade.pais = pais.sigla_id;

-- Questão 67)

SELECT funcionario.nome , COUNT(*)
FROM pedido
INNER JOIN funcionario
ON funcionario.codigo_id = pedido.vendedor
GROUP BY pedido.vendedor
ORDER BY count(*) desc;

-- Questão 69)

select produto.nome, itens_pedido.quant, itens_pedido.total
from produto
inner join itens_pedido
on itens_pedido.produto_id = produto.codigo_id
inner join pedido
on itens_pedido.pedido_id = pedido.codigo_id
where year(pedido.data_pedido) = '2016'
order by total desc;

-- Questão 71) 

SELECT pedido.codigo_id, clientes.contato, pedido.situacao
FROM pedido
INNER JOIN (SELECT cliente.contato, cliente.codigo_id FROM cliente 
INNER JOIN cidade ON cliente.cidade = cidade.codigo_id WHERE cidade.pais != 'BR')
 AS clientes
ON pedido.cliente = clientes.codigo_id
WHERE pedido.situacao != 'T';

-- Questão 73) 

select t1.nome, t2.total, t2.total * 100 / t.soma as percentual
from 
	(select produto.nome as nome from produto 
    inner join itens_pedido on itens_pedido.produto_id = produto_id) as t1
inner join
	(select pedido.total as total from pedido
    inner join itens_pedido on itens_pedido.pedido_id = pedido.codigo_id
    where year(pedido.data_pedido) = '2016') as t2 
cross join
	(select sum(total) as soma from pedido) as t
order by t2.total desc;

-- Questão 75) 

select setor.nome, count(setor) * 100.0 / t.total as percentual
from funcionario
inner join setor
on funcionario.setor = setor.codigo_id
cross join
	(select count(*) as total from funcionario) as t
group by setor
order by percentual;

-- Questão 77)

select  funcao.nome, sum(funcionario.salario) + sum(funcao.salario) as 'folha de pagamento'
from funcionario
inner join funcao
on funcionario.funcao = funcao.codigo_id
group by funcao;

-- Questão 79)

SELECT nome, salario FROM funcao WHERE salario = (SELECT MAX(salario) FROM funcao);

-- Questão 81)

SELECT AVG(YEAR(current_date()) - YEAR(funcionario.data_nascimento))
FROM funcionario
INNER JOIN funcao ON funcionario.funcao = funcao.codigo_id
WHERE funcionario.funcao = (SELECT codigo_id FROM funcao WHERE salario = (SELECT max(salario) FROM funcao));

-- Questão 83)

SELECT nome, admissao FROM funcionario
WHERE codigo_id = (SELECT codigo_id FROM funcionario ORDER BY admissao ASC LIMIT 1);

-- Questão 85)

SELECT COUNT(*) AS 'Funcionarios com ramal'
FROM (SELECT codigo_id FROM funcionario WHERE ramal_individual IS NOT NULL) AS f;

-- Questão 87)

select cliente.razao_social, sum(pedido.total) as total, sum(pedido.total) * 100.0 / t.total as percentual
from cliente
inner join pedido
on pedido.cliente = cliente.codigo_id
cross join
	(select sum(pedido.total) as total from pedido) as t
group by pedido.cliente
order by percentual;

-- Questão 89) 

select pais.nome
from pais where pais.sigla_id = (select cidade.pais 
from cidade where cidade.codigo_id = (select cliente.cidade
from cliente where cliente.codigo_id = 
(select pedido.cliente
from pedido
group by pedido.cliente
order by count(*) desc
limit 1)));

-- Questão 91) 

select cliente.razao_social
from cliente where cliente.codigo_id = (select pedido.cliente
from pedido
group by pedido.cliente
order by count(*) asc
limit 1);

-- Questão 93)

INSERT INTO `pais` (`sigla_id`, `nome`) VALUES ('AU', 'AUSTRÁLIA');
INSERT INTO `uf` (`sigla_id`, `pais_id`, `nome`) VALUES ('GB', 'AU', 'GIBSON'), ('VT', 'AU', 'VITORIA');
INSERT INTO `cidade` (`codigo_id`, `nome`, `uf`, `pais`) VALUES (NULL, 'Sidney', 'GB', 'AU'), (NULL, 'Melbourne', 'GB', 'AU'), (NULL, 'Albany', 'VT', 'AU'), (NULL, 'Adelaide', 'VT', 'AU');

-- Questão 95)

UPDATE cliente
SET cliente.cidade = (SELECT MAX(cidade.codigo_id) FROM cidade)
WHERE cliente.razao_social = 'MOVEIS'
and cliente.codigo_id > 0; -- burlar o safe mode:
