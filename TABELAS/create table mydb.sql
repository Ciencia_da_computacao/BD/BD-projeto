create database mydb;

use mydb;

-- funcao
CREATE TABLE funcao
(
    codigo_id INT      AUTO_INCREMENT,
    nome      CHAR(50) NULL,
    salario   NUMERIC(16,2)    NULL,

    CONSTRAINT pk_funcao_codigo PRIMARY KEY(codigo_id)
);
-- pais
CREATE TABLE pais
(
    sigla_id CHAR(2)  NOT NULL,
    nome     CHAR(40) NULL,

    CONSTRAINT pk_pais_sigla PRIMARY KEY(sigla_id)
);


-- uf
CREATE TABLE uf
(
    sigla_id CHAR(2)  NOT NULL,
    pais_id  CHAR(2)  NOT NULL,
    nome     CHAR(30) NOT NULL,

    CONSTRAINT pk_uf_sigla PRIMARY KEY(sigla_id, pais_id),
    CONSTRAINT fk_uf_pais  FOREIGN KEY(pais_id) REFERENCES pais(sigla_id)
);
-- cidade
CREATE TABLE cidade
(
    codigo_id INT      AUTO_INCREMENT,
    nome      CHAR(40) NULL,
    uf        CHAR(2)  NOT NULL,
    pais      CHAR(2)  NOT NULL,

    CONSTRAINT pk_cidade_codigo PRIMARY KEY(codigo_id),
    CONSTRAINT fk_cidade_uf     FOREIGN KEY(uf, pais) REFERENCES uf(sigla_id, pais_id),
    CONSTRAINT fk_cidade_pais   FOREIGN KEY(pais) REFERENCES pais(sigla_id)
);

-- cliente
CREATE TABLE cliente
(
    codigo_id       SMALLINT AUTO_INCREMENT,
    cgc_cpf         CHAR(15)               NOT NULL,
    tipo            CHAR(1)                NOT NULL
        CHECK (tipo = 'F' or tipo = 'J'),
    razao_social    CHAR(30)               NOT NULL,
    tipo_logradouro CHAR(3)                NOT NULL
        CHECK (tipo_logradouro = 'AVE' OR tipo_logradouro = 'RUA' OR tipo_logradouro = 'PRA' OR tipo_logradouro = 'TRA'
               OR tipo_logradouro = 'TRA' OR tipo_logradouro = 'ROD' OR tipo_logradouro = 'VIL'),
    logradouro      CHAR(30)               NOT NULL,
    complemento     CHAR(20)               NULL,
    bairro          CHAR(20)               NOT NULL,
    cidade          INT                    NOT NULL,
    cep             CHAR(8)                NOT NULL,
    fone            CHAR(14)               NOT NULL,
    contato         CHAR(30)               NOT NULL,
    fax             CHAR(14)               NULL,
    insc_est        CHAR(16)               NULL,
    email           CHAR(40)               NULL,
    obs             TEXT                   NULL,

    CONSTRAINT pk_cliente_codigo PRIMARY KEY(codigo_id),
    CONSTRAINT cgc_cpf_unique    UNIQUE(cgc_cpf),
    CONSTRAINT fk_cliente_cidade FOREIGN KEY(cidade)   REFERENCES cidade(codigo_id)
);

-- tipo_produto
CREATE TABLE tipo_produto
(
    codigo_id SMALLINT AUTO_INCREMENT,
    nome      CHAR(50) NULL,

    CONSTRAINT pk_tipo_produto_codigo PRIMARY KEY(codigo_id)
);

-- produto
CREATE TABLE produto
(
    codigo_id       SMALLINT              AUTO_INCREMENT,
    nome            CHAR(40)              NOT NULL,
    tipo            SMALLINT              NOT NULL,
    unidade         CHAR(3)               NOT NULL,
    estoque_real    NUMERIC(16,3)         NULL,
    estoque_virtual NUMERIC(16,3)         NULL,
    estoque_minimo  NUMERIC(16,3)         NULL,
    custo           NUMERIC(16,2)         NULL,
    venda           NUMERIC(16,2)         NULL,

    CONSTRAINT pk_produto_codigo PRIMARY KEY(codigo_id),
    CONSTRAINT fk_produto_tipo   FOREIGN KEY(tipo) REFERENCES tipo_produto(codigo_id)
);

-- funcionario
CREATE TABLE funcionario
(
    codigo_id        INT           AUTO_INCREMENT,
    nome             CHAR(30)      NOT NULL,
    sexo             CHAR(1)       NOT NULL
        CHECK (sexo = 'M' OR sexo = 'F'),
    setor            SMALLINT      NOT NULL,
    ramal_individual CHAR(4)       NOT NULL,
    estado_civil     CHAR(1)       NOT NULL
        CHECK (estado_civil = 'S' OR estado_civil = 'C' OR estado_civil = 'D'
               OR estado_civil = 'V' OR estado_civil = 'O'),
    data_nascimento  DATETIME NOT NULL,
    rg_numero        CHAR(15)      NULL,
    /*nacionalidade    CHAR(2)       NOT NULL,*/
    naturalidade     INT           NOT NULL,
    cpf              CHAR(11)      NULL,
    sangue_fator     CHAR(2)       NULL,
    sangue_rh        CHAR(1)       NULL,
    tipo_logradouro  CHAR(3)       NULL
        CHECK (tipo_logradouro = 'AVE' OR tipo_logradouro = 'RUA' OR tipo_logradouro = 'PRA' OR tipo_logradouro = 'TRA'
               OR tipo_logradouro = 'TRA' OR tipo_logradouro = 'ROD' OR tipo_logradouro = 'VIL'),
    logradouro       CHAR(30)      NOT NULL,
    complemento      CHAR(30)      NULL,
    bairro           CHAR(20)      NOT NULL,
    cidade           INT           NOT NULL,
    cep              CHAR(8)       NULL,
    fone             CHAR(14)      NULL,
    funcao           INT           NOT NULL,
    admissao         DATETIME      NOT NULL,
    email            CHAR(40)      NULL,
    salario          NUMERIC(16,2)         NOT NULL,

    CONSTRAINT pk_funcionario_codigo       PRIMARY KEY(codigo_id),
    CONSTRAINT fk_funcionario_naturalidade FOREIGN KEY(naturalidade) REFERENCES cidade(codigo_id),
    CONSTRAINT fk_funcionario_cidade       FOREIGN KEY(cidade)       REFERENCES cidade(codigo_id),
    CONSTRAINT fk_funcionario_funcao       FOREIGN KEY(funcao)       REFERENCES funcao(codigo_id),
    CONSTRAINT cpf_unique                  UNIQUE(cpf),
    CONSTRAINT rg_unique                   UNIQUE(rg_numero)
);

-- pedido
CREATE TABLE pedido
(
    codigo_id   INT           AUTO_INCREMENT,
    cliente     SMALLINT      NOT NULL,
    data_pedido DATETIME NOT NULL,
    total       NUMERIC(16,2)         NULL,
    situacao    CHAR(1)       NULL
        CHECK (situacao = 'A' OR situacao = 'P' OR situacao = 'T'),
    vendedor    INT           NULL,

    CONSTRAINT pk_pedido_codigo   PRIMARY KEY(codigo_id),
    CONSTRAINT fk_pedido_cliente  FOREIGN KEY(cliente)  REFERENCES cliente(codigo_id),
    CONSTRAINT fk_pedido_vendedor FOREIGN KEY(vendedor) REFERENCES funcionario(codigo_id)
);


-- setor
CREATE TABLE setor
(
    codigo_id   SMALLINT AUTO_INCREMENT,
    nome        CHAR(50) NULL,
    sigla       CHAR(10) NULL,
    ramal       CHAR(3)  NULL,
    superior    SMALLINT NULL,
    responsavel INT      NULL,

    CONSTRAINT pk_setor_codigo      PRIMARY KEY(codigo_id),
    CONSTRAINT fk_setor_responsavel FOREIGN KEY(responsavel) REFERENCES funcionario(codigo_id),
    CONSTRAINT fk_setor_superior    FOREIGN KEY(superior) REFERENCES setor(codigo_id)
);

-- intens_pedido
CREATE TABLE itens_pedido
(
    pedido_id  INT           NOT NULL,
    produto_id SMALLINT      NOT NULL,
    quant      NUMERIC(10,3) NOT NULL,
    total      NUMERIC(16,2) NULL,
    situacao   CHAR(1)       NULL
        CHECK (situacao = 'A' OR situacao = 'P' OR situacao = 'T'),

    CONSTRAINT pk_itens_pedidos         PRIMARY KEY(pedido_id, produto_id),
    CONSTRAINT fk_itens_pedido_pedido  FOREIGN KEY(pedido_id)  REFERENCES pedido(codigo_id),
    CONSTRAINT fk_itens_pedido_produto FOREIGN KEY(produto_id) REFERENCES produto(codigo_id)
);
