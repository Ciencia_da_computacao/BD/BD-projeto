/*Mude a cidade dos clientes para a cidade mais recentemente cadastrada (maior código). 
Faça isso apenas para os clientes cujo nome contem a palavra “MOVEIS”.
*/

use mydb2;

select * from cliente;
select * from cidade;

update cliente
set cliente.cidade = (select max(cidade.codigo_id) from cidade)
where cliente.razao_social = 'MOVEIS';



