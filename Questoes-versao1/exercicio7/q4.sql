/*4 Apague o produto cujo código é 2.
Observe que os itens do pedido 2 ficaram órfãos (sem os dados do
pedido).
Isso foi permitido pelo database pelo fato de não haver regras de
referência (foreign keys) estabelecidas.*/

use mydb2;

select * from produto;
select * from itens_pedido;

delete from itens_pedido where itens_pedido.produto_id = 2;
delete from produto where produto.codigo_id = 2;