/*Atualize os preços dos produtos com um aumento de 10%, exceto
aqueles do tipo "MATERIA PRIMA RECICLADA"
*/

use mydb2;

select * from tipo_produto;
select * from produto;


update produto
inner join tipo_produto on produto.tipo = tipo_produto.codigo_id
set produto.venda = produto.venda*1.1
where tipo_produto.nome != 'MATERIA PRIMA RECICLADA';