-- Qual a data de admissão do funcionário mais antigo
-- da empresa ?

use mydb;

-- resposta
select min(admissao)from funcionario;

-- retornando todos os campos
select * from funcionario order by admissao asc limit 1;