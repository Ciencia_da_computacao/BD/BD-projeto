-- Em que mês foi feito o pedido mais
-- antigo que ainda não foi atendido ?

use mydb;

select * from pedido;

-- da pergunta
select month(min(data_pedido)) as 'Mes antigo' from pedido
where situacao = 'A';

-- extra: que foi atendido
select month(min(data_pedido)) as 'Mes antigo' from pedido
where situacao in ('P', 'T');