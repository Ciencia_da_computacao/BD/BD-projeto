-- Qual a média de idade em anos dos funcionários
-- do sexo feminino ?

select avg(year(current_date()) - year(data_nascimento))
as 'Media feminina'
from mydb.funcionario
where sexo = 'F';

select * from mydb.funcionario where sexo = 'F';