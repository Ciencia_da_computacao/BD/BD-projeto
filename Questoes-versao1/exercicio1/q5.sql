-- criar coluna
alter table mydb.produto add produto.projecao numeric(16,2) null;

-- colocar os valores
update mydb.produto set produto.projecao = produto.venda * 1.35 where produto.codigo_id > 0;

-- verificar
select * from mydb.produto;