-- Mostre a lista de produtos contendo:
-- código, preço de venda, estoque real,
-- código do tipo e nome do
-- tipo do produto

use mydb2;

select produto.codigo_id as 'Codigo do produto',
	produto.venda as 'Preço de venda',
    produto.estoque_real as 'Estoque real',
    produto.tipo as 'Código do tipo',
    tipo_produto.nome as 'Nome do tipo'
from produto
inner join tipo_produto on 
	produto.tipo = tipo_produto.codigo_id;