-- Mostre o ranking dos vendedores, considerando o valor de todos os pedidos de cada vendedor (nome
-- do vendedor, total dos pedidos)

use mydb;

select * from pedido;
select * from funcionario;

select funcionario.nome , count(*)
from pedido
inner join funcionario
on funcionario.codigo_id = pedido.vendedor
group by pedido.vendedor
order by count(*) desc;