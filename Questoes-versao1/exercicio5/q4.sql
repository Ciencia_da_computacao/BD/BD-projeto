-- Mostre a média dos preços de venda 
-- e do estoque dos produtos, agrupados por tipo de produto,
-- mostrando o nome do tipo de produto

use mydb2;

select venda,estoque_real,tipo from produto;
select * from tipo_produto;

select avg(produto.venda) as 'Media de Venda',
	avg(produto.estoque_real) as 'Media do Estoque',
	tipo_produto.nome as 'Nome do Tipo'
from produto
inner join tipo_produto on produto.tipo=tipo_produto.codigo_id
group by tipo_produto.nome;