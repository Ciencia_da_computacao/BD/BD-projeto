-- Mostre o nome do funcionário e o ramal do setor onde está lotado. Caso o funcionário tenha ramal
-- individual, este deverá prevalecer. (Dica: use a função ISNULL(expr1, expr2), 
-- onde expr2 é retornado
-- quando expr1 for nulo

use mydb;

select * from setor;
select * from funcionario;

select funcionario.nome , ifnull(funcionario.ramal_individual, setor.ramal)
from funcionario
inner join setor
on funcionario.setor = setor.codigo_id
;