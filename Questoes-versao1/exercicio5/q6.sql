-- 6. Mostre o valor total em dinheiro dos produtos vendidos no
-- ano passado, agrupados por produto

use mydb2;

select
	sum(itens_pedido.total) as 'Valor total',
	-- itens_pedido.pedido_id
	itens_pedido.produto_id
	-- itens_pedido.total,
 	-- itens_pedido.quant
    
    -- pedido.codigo_id,
    -- pedido.data_pedido,
    -- pedido.total
    
from itens_pedido
inner join pedido on itens_pedido.pedido_id = pedido.codigo_id
where year(pedido.data_pedido)=year(current_date())-1
group by itens_pedido.produto_id;

