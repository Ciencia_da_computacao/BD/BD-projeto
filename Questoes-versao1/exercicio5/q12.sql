-- Mostre a lista de nomes de funcionários, o nome da sua função, 
-- o seu salário base, o salário da função e a soma dos dois

use mydb;

select * from funcao;
select * from funcionario;

select funcionario.nome , funcao.nome as 'Funcao nome' , funcionario.salario as 'Funcio salario', funcao.salario as 'Funcao salario',
funcionario.salario + funcao.salario as 'soma'
from funcionario
inner join funcao
on funcionario.funcao = funcao.codigo_id;