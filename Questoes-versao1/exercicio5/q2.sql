-- Mostre o nome do funcionário, o nome da cidade onde nasceu

use mydb2;

select naturalidade from funcionario;
select nome from cidade;

select funcionario.nome,
	cidade.nome
from funcionario
inner join cidade on funcionario.naturalidade=cidade.codigo_id;