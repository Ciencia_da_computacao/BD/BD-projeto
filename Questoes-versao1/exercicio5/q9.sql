-- Mostre os setores e o nome do setor superior (dica: faça um
-- join da tabela de setores com ela mesma, mas usando dois apelidos diferentes para cada uma delas)

use mydb;

select setor from funcionario;
select * from setor;

select setor1.nome as 'NOme do filho', setor2.nome as 'NOme do superior'
from setor as setor1
inner join setor as setor2
on setor2.codigo_id = setor1.superior;