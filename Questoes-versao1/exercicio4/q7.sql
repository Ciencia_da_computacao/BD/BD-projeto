-- Mostre o valor total em dinheiro dos pedidos 
-- feitos no ano passado, agrupados por cliente

use mydb;

select * from pedido;
-- resposta
select sum(total), cliente from pedido 
where year(data_pedido) = year(current_date()) - 1
group by cliente;