-- Mostre o valor da folha de pagamento da empresa
-- (considerando apenas os salários da tabela de
-- funcionários), agrupado por função
use mydb;

select sum(salario) as 'Folha de pagamento', funcao
from funcionario
group by funcao;