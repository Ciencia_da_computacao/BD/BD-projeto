-- 16. Faça o mesmo que o exercício anterior,
-- mas agrupando por cidade, sexo e setor

use mydb;

select cidade,
	setor,
    sexo,
 	count(codigo_id) as 'Funcionarios'
from funcionario
group by cidade, sexo, setor
order by cidade, setor, sexo;

select cidade,
	setor,
    sexo
from funcionario;