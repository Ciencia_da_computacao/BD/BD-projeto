-- Mostre a cidade e a quantidade de funcionários que nasceram em
-- cada cidade, considerando apenas a cidade natal 
-- (naturalidade) indicada na tabela de funcionários

use mydb;

select * from funcionario;

-- resposta
select naturalidade, count(naturalidade) as 'Quantidade' 
from funcionario group by naturalidade;