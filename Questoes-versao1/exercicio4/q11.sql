-- Mostre a quantidade de homens e mulheres nascidos em cada
-- cidade existente na tabela de
-- funcionários

use mydb;

select count(codigo_id) as 'contador',
	sexo, naturalidade
from funcionario
group by sexo, naturalidade;

select sexo, naturalidade
from funcionario;
