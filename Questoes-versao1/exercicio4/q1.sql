-- Mostre o valor da folha de pagamento da empresa
-- (total de salários), considerando apenas os salários
-- da tabela de funcionários, agrupado por setor

use mydb;

select sum(salario) as 'Total de salario', setor from funcionario
group by setor;

select salario, setor from funcionario
order by setor;