-- 16. Quais os funcionários que nasceram entre dezembro/1965 
-- e Fevereiro/1966 na cidade cujo código é 10 ?
use mydb;

-- inserindo um que funcione
insert into mydb.funcionario (nome, sexo, setor, ramal_individual, estado_civil, data_nascimento, rg_numero,
                        naturalidade, cpf, sangue_fator, sangue_rh, tipo_logradouro, logradouro,
                        complemento, bairro, cidade, cep, fone, funcao, admissao, email, salario)
values ('Luiz Henrique', 'M', 1, '0800', 'S', '1966-01-18 10:34:09',
            '----sd---', 3, '09414263475', 'AB', '+', 'RUA', 'Rua das Moitas Ardentes',
            'Casa', 'Centro', 4, '58200000', '08002148965237', 2, '2015-06-18 10:34:09',
            'cobra@ig.com',3500);

select * from funcionario;


select * from funcionario where data_nascimento between
'1965-12-01 00:00:00'   and '1966-02-28 23:59:59';