-- Mostre a sigla e o nome dos estados que não são brasileiros, ordenados pela sigla e nome em ordem
-- crescente

use mydb;

select uf from cidade where pais != 'BR'
order by uf desc;