-- 14. Mostre o nome da empresa, o contato e o e-mail dos clientes que têm e-mail.

INSERT INTO cliente (cgc_cpf, tipo, razao_social, tipo_logradouro, logradouro, complemento,
                     bairro, cidade, cep, fone, contato)
VALUES
('102.429.218-15','F','Carlos Garçon','AVE','Epitacio Pessoa','N 666, APT 606','Sao Jose',2,58000001,'8332315044','8399123123');

select email from cliente;

select razao_social,contato from cliente where email is not null;