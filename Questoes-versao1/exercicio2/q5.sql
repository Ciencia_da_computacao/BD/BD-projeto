-- Faça uma projeção no nome e salário dos funcionários dos setores 3 e 10 e crie uma coluna “aumento” na
-- tabela de resultado mostrando como ficaria seu salário com 20% de aumento (considere o salário do
-- funcionário como sendo apenas o da tabela funcionario)

use mydb;

-- criar a coluna
alter table funcionario add aumento numeric(16,2) null;

-- atualizar o campo
update funcionario set aumento = salario*1.2 where codigo_id>0;

-- exibir dos funcionarios 3 e 10
SELECT nome, salario, setor, aumento from funcionario where setor = 3 or setor = 10;