-- Mostre os atributos dos estados do Brasil (sigla=”BR”) ordenados pelo nome em ordem decrescente
use mydb;

-- mostra os estados do brasil
select distinct uf from cidade where pais = 'BR'
order by uf desc;