-- Mostre o nome dos funcionários homens admitidos este ano,
-- exceto aqueles cujos nomes iniciam com as
-- letras A, B ou C.

use mydb;

select nome from funcionario where sexo = 'M'
and Year(funcionario.admissao) = 2012;

select nome from funcionario where sexo = 'M'
and nome regexp '[^ABC]'
and Year(funcionario.admissao) = year(current_date());