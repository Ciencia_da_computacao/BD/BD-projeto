-- 15. Mostre há quantos anos os funcionários estão empregados
use mydb;

select nome,year(current_date())-year(admissao) as 'Tempo Empregado' from funcionario;