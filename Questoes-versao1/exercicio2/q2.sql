-- Mostre o nome e o setor dos funcionários que nasceram na cidades cujo código é 12 ou 13, ordenado pelo
-- setor, depois pelo nome

select funcionario.nome, funcionario.setor from mydb.funcionario
where cidade = 13 or cidade = 12
order by setor, nome;